# Die Beobachterposition

Leidenschaftslosigkeit, einen Schritt aus sich heraus tun und sich selbst beobachten.
