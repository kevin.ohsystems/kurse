# Bedeutung von Emotionalität im professionellen Handeln

Gefühle haben immer einen Einfluss auf das Verhalten von Personen und sie beeinflussen sich gegenseitig. Wenn jemand im Team schlecht gelaunt oder gelangweilt ist, beeinflusst das alle. Unsere eigenen Emotionen beeinflussen auch, wie wir Lernen oder — wenn sie uns negativ beeinflussen — nicht Lernen. Darum ist es wichtig, einen guten Umgang mit den Gefühlen zu finden; mit unseren eigenen wie auch mit denen der anderen. Was aber ist "ein guter Umgang"?
