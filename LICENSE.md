# License
GNU All-Permissive License

## License Notice
Copying and distribution of this project, with or without modification, are permitted in any medium without royalty provided the copyright notice and this notice are preserved. This project is offered as-is, without any warranty.
