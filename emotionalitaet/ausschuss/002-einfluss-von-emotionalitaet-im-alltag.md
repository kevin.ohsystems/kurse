# Was bedeutet das konkret für unseren Alltag?

Emotionen haben einen starken Einfluss auf unser Verhalten. Wenn du dich fragst "warum verhaltet die sich jetzt so?" oder "warum habe ich jetzt so reagiert" sagst du dir vielleicht "weil ich oder sie gereizt, wütend oder traurig war"; das sind Emotionen die unser Verhalten beeinflussen.
