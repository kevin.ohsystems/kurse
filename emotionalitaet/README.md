# Selbstreflexion emotionaler Aspekte der Erwachsenenbildung

**Work in Progress**

## Onlinekurs im Selbststudium

### Information

Dieser Kurs orientiert sich lose am Buch "Die emotionale Konstruktion der Wirklichkeit" von Rolf Arnold in der Reihe "Beiträge zu einer emotionspädagogischen Erwachsenenbildung", erschienen 2005 im Schneider Verlag Hohengehren GmbH.

Erstellt von Kevin Mueller, Innovation Fellow für Selbstorganisiertes Lernen, Smart City Zürich 2021

### Sinn und Zweck

Dieser Kurs zielt auf ein gestärktes Verständnis der emotionalen Aspekte in der Erwachsenenbildung.
